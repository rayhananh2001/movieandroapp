package com.example.movieandro.utils

import com.example.movieandro.R
import com.example.movieandro.data.ShowEntity
import java.util.ArrayList

object DataDummy {
    // Untuk menambah data film
    fun generateDummyMovie(): ArrayList<ShowEntity> {
        val movies = ArrayList<ShowEntity>()

        movies.add(
            ShowEntity(
                "M1",
                R.drawable.poster_alita,
                "Alita The Battle Robot",
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Vel eros donec ac odio tempor orci."
            )
        )

        movies.add(
            ShowEntity(
                "M2",
                R.drawable.poster_aquaman,
                "Aquaman",
                "At ultrices mi tempus imperdiet nulla malesuada pellentesque. Id volutpat lacus laoreet non curabitur gravida arcu."
            )
        )

        movies.add(
            ShowEntity(
                "M3",
                R.drawable.poster_creed,
                "Creed",
                "Neque vitae tempus quam pellentesque nec nam. Erat pellentesque adipiscing commodo elit at."
            )
        )

        movies.add(
            ShowEntity(
                "M4",
                R.drawable.poster_bohemian,
                "Bohemian",
                "Dignissim cras tincidunt lobortis feugiat vivamus at augue. Condimentum vitae sapien pellentesque habitant."
            )
        )

        movies.add(
            ShowEntity(
                "M5",
                R.drawable.poster_a_start_is_born,
                "A Star is born",
                "Amet consectetur adipiscing elit ut aliquam purus sit. A diam maecenas sed enim."
            )
        )

        movies.add(
            ShowEntity(
                "M6",
                R.drawable.poster_glass,
                "glass",
                "Scelerisque viverra mauris in aliquam sem fringilla ut morbi tincidunt."
            )
        )

        movies.add(
            ShowEntity(
                "M7",
                R.drawable.poster_crimes,
                "Crimes",
                "Ut diam quam nulla porttitor massa id neque. Ut ornare lectus sit amet est placerat."
            )
        )

        movies.add(
            ShowEntity(
                "M8",
                R.drawable.poster_how_to_train,
                "How to train your dragons",
                "Ac tortor dignissim convallis aenean et tortor at risus viverra. Arcu ac tortor dignissim convallis."
            )
        )

        movies.add(
            ShowEntity(
                "M9",
                R.drawable.poster_cold_persuit,
                "Cold Pursuit",
                "Facilisi morbi tempus iaculis urna id volutpat lacus. Et egestas quis ipsum suspendisse ultrices gravida dictum fusce."
            )
        )

        movies.add(
            ShowEntity(
                "M10",
                R.drawable.poster_infinity_war,
                "Infinity War",
                "Mus mauris vitae ultricies leo integer malesuada nunc vel risus. "

            )
        )

        movies.add(
            ShowEntity(
                "M11",
                R.drawable.poster_marry_queen,
                "Merry Queen",
                "Gravida in fermentum et sollicitudin ac. Vitae auctor eu augue ut lectus arcu bibendum at varius."
            )
        )

        movies.add(
            ShowEntity(
                "M12",
                R.drawable.poster_master_z,
                "Master Z",
                "Non enim praesent elementum facilisis. Lacinia quis vel eros donec ac odio tempor orci dapibus."
            )
        )

        return movies
    }

    // untuk menambah data acara tv
    fun generateDummyTvShow(): ArrayList<ShowEntity> {
        val tvShow = ArrayList<ShowEntity>()

        tvShow.add(
            ShowEntity(
                "TV1",
                R.drawable.poster_v_arrow,
                "Arrow",
                "Venenatis tellus in metus vulputate eu scelerisque felis. Etiam erat velit scelerisque in dictum."
            )
        )

        tvShow.add(
            ShowEntity(
                "TV2",
                R.drawable.poster_v_god,
                "God",
                "Sit amet nulla facilisi morbi tempus iaculis urna id. Risus nullam eget felis eget nunc lobortis mattis aliquam."
            )
        )

        tvShow.add(
            ShowEntity(
                "TV3",
                R.drawable.poster_dragon_ball,
                "Dragon Ball",
                "Viverra accumsan in nisl nisi scelerisque eu ultrices vitae auctor."
            )
        )

        tvShow.add(
            ShowEntity(
                "TV4",
                R.drawable.poster_family_guy,
                "Family Guy",
                "Vulputate odio ut enim blandit volutpat. Imperdiet proin fermentum leo vel orci porta non pulvinar."
            )
        )

        tvShow.add(
            ShowEntity(
                "TV5",
                R.drawable.poster_v_ncis,
                "NCIS",
                "Fames ac turpis egestas maecenas pharetra. Morbi quis commodo odio aenean sed."
            )
        )

        tvShow.add(
            ShowEntity(
                "TV6",
                R.drawable.poster_v_hanna,
                "Hanna",
                "Amet porttitor eget dolor morbi non. Diam vel quam elementum pulvinar etiam non."
            )
        )

        tvShow.add(
            ShowEntity(
                "TV7",
                R.drawable.poster_v_the_simpson,
                "The Simpsons",
                "Sit amet nulla facilisi morbi tempus iaculis urna id volutpat. Nunc pulvinar sapien et ligula ullamcorper malesuada proin."
            )
        )

        tvShow.add(
            ShowEntity(
                "TV8",
                R.drawable.poster_v_supergirl,
                "Supergirl",
                "Eget est lorem ipsum dolor sit amet. Sed lectus vestibulum mattis ullamcorper velit sed ullamcorper morbi."
            )
        )

        tvShow.add(
            ShowEntity(
                "TV9",
                R.drawable.poster_v_shameless,
                "Shameless",
                "Tellus in metus vulputate eu scelerisque felis imperdiet proin fermentum. Sed faucibus turpis in eu mi bibendum neque egestas congue."
            )
        )

        tvShow.add(
            ShowEntity(
                "TV10",
                R.drawable.poster_v_the_walking_dead,
                "The Walking Dead",
                "Orci ac auctor augue mauris augue neque. Bibendum est ultricies integer quis auctor elit. "
            )
        )

        tvShow.add(
            ShowEntity(
                "TV11",
                R.drawable.poster_v_the_umbrella,
                "The Umbrella",
                "Eget mauris pharetra et ultrices neque ornare aenean."
            )
        )

        tvShow.add(
            ShowEntity(
                "TV12",
                R.drawable.poster_v_riverdale,
                "The Riverdale",
                "Morbi non arcu risus quis varius quam. Massa tincidunt nunc pulvinar sapien et."
            )
        )

        return tvShow
    }

    // gabung data film dan acara tv
    fun generateAllShow(): ArrayList<ShowEntity> {
        val allShow = ArrayList<ShowEntity>()

        allShow.addAll(generateDummyMovie())
        allShow.addAll(generateDummyTvShow())

        return allShow
    }
}