package com.example.movieandro.ui.tvshow

import com.example.movieandro.data.ShowEntity

interface TvShowRecyclerViewListener {
    fun onItemClicked(tvShow: ShowEntity)
}