package com.example.movieandro.ui.tvshow

import androidx.lifecycle.ViewModel
import com.example.movieandro.data.ShowEntity
import com.example.movieandro.utils.DataDummy

class TvShowViewModel : ViewModel() {

    fun getTvShows(): List<ShowEntity> = DataDummy.generateDummyTvShow()
}