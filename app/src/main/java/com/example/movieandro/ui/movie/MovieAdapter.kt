package com.example.movieandro.ui.movie

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.movieandro.data.ShowEntity
import com.example.movieandro.databinding.ListItemMovieBinding
import java.util.ArrayList

class MovieAdapter : RecyclerView.Adapter<MovieAdapter.ViewHolder>() {

    private var listMovies = ArrayList<ShowEntity>()
    lateinit var listener: MovieRecyclerViewListener

    inner class ViewHolder(private val binding: ListItemMovieBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(movie: ShowEntity) {
            with(binding) {
                imgPoster.setImageResource(movie.imagePath)
                tvTitle.text = movie.title
                tvDesc.text = movie.desc
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ListItemMovieBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(listMovies[position])

        holder.itemView.setOnClickListener {
            listener.onItemClicked(listMovies[position])
        }
    }

    override fun getItemCount(): Int = listMovies.size

    fun setMovies(movies: List<ShowEntity>?) {
        if (movies == null) return
        this.listMovies.clear()
        this.listMovies.addAll(movies)
    }
}