package com.example.movieandro.ui.movie

import androidx.lifecycle.ViewModel
import com.example.movieandro.data.ShowEntity
import com.example.movieandro.utils.DataDummy

class MovieViewModel : ViewModel() {
    // ngambil data film dari dummy
    fun getMovies(): List<ShowEntity> = DataDummy.generateDummyMovie()
}