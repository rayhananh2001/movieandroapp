package com.example.movieandro.ui.home

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.movieandro.databinding.ActivityHomeBinding

class HomeActivity : AppCompatActivity() {

    private lateinit var binding: ActivityHomeBinding
    private lateinit var sectionsPagerAdapter: SectionsPagerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // bikin adapter buat view pager
        sectionsPagerAdapter = SectionsPagerAdapter(this, supportFragmentManager)
        binding.vpHome.adapter = sectionsPagerAdapter
        // set tab home dengan view pager
        binding.tabHome.setupWithViewPager(binding.vpHome)

        supportActionBar?.elevation = 0f
    }
}