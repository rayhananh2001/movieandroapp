package com.example.movieandro.ui.movie

import com.example.movieandro.data.ShowEntity

interface MovieRecyclerViewListener {
    fun onItemClicked(movie: ShowEntity)
}