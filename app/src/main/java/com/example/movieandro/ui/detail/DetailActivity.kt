package com.example.movieandro.ui.detail

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import com.example.movieandro.data.ShowEntity
import com.example.movieandro.databinding.ActivityDetailBinding


class DetailActivity : AppCompatActivity() {

    companion object {
        const val EXTRA_SHOW = ""
    }

    private lateinit var binding: ActivityDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val viewModel = ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        )[DetailViewModel::class.java]

        val extras = intent.extras

        if (extras != null) {
            val showId = extras.getString(EXTRA_SHOW)
            if (showId != null) {
                // isi show id ama ngambil data acara sesuai id
                viewModel.setSelectedShow(showId)
                populateDetail(viewModel.getShow() as ShowEntity)
            }
        }
    }

    private fun populateDetail(showEntity: ShowEntity) {
        binding.imgPoster.setImageResource(showEntity.imagePath)
        binding.tvTitle.text = showEntity.title
        binding.tvDesc.text = showEntity.desc
    }
}