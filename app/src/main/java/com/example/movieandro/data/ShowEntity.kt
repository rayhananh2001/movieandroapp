package com.example.movieandro.data

data class ShowEntity(
    var movieId: String,
    var imagePath: Int,
    var title: String,
    var desc: String
)
