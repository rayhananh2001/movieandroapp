package com.example.movieandro.ui.movie

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.movieandro.data.ShowEntity
import com.example.movieandro.databinding.FragmentMovieBinding
import com.example.movieandro.ui.detail.DetailActivity
import splitties.fragments.start


class MovieFragment : Fragment(), MovieRecyclerViewListener {

    private lateinit var binding: FragmentMovieBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentMovieBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (activity != null) {
            // inisialisasi view model dan list film yang diambil
            val viewModel = ViewModelProvider(
                this,
                ViewModelProvider.NewInstanceFactory()
            )[MovieViewModel::class.java]
            val movies = viewModel.getMovies()

            // inisialisasi adapter dengan disikan data dari view model
            val movieAdapter = MovieAdapter()
            movieAdapter.setMovies(movies)

            with(binding.rvMovie) {
                layoutManager = LinearLayoutManager(context)
                setHasFixedSize(true)
                adapter = movieAdapter

                movieAdapter.listener = this@MovieFragment
            }
        }
    }

    private fun showSelectedMovie(movie: ShowEntity) {
        start<DetailActivity> {
            putExtra(DetailActivity.EXTRA_SHOW, movie.movieId)
        }
    }

    override fun onItemClicked(movie: ShowEntity) {
        showSelectedMovie(movie)
    }
}