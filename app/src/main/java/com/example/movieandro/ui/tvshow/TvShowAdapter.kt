package com.example.movieandro.ui.tvshow

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.movieandro.data.ShowEntity
import com.example.movieandro.databinding.ListItemTvShowBinding

class TvShowAdapter : RecyclerView.Adapter<TvShowAdapter.ViewHolder>() {

    private var listTvShows = ArrayList<ShowEntity>()
    lateinit var listener: TvShowRecyclerViewListener

    inner class ViewHolder(private val binding: ListItemTvShowBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(tvShow: ShowEntity) {
            with(binding) {
                imgPoster.setImageResource(tvShow.imagePath)
                tvTitle.text = tvShow.title
                tvDesc.text = tvShow.desc
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ListItemTvShowBinding.inflate(
                LayoutInflater.from(
                    parent.context
                ),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(listTvShows[position])

        holder.itemView.setOnClickListener {
            listener.onItemClicked(listTvShows[position])
        }
    }

    override fun getItemCount(): Int = listTvShows.size

    fun setTvShows(tvShow: List<ShowEntity>?) {
        if (tvShow == null) return
        this.listTvShows.clear()
        this.listTvShows.addAll(tvShow)
    }
}