package com.example.movieandro.ui.tvshow

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.movieandro.data.ShowEntity
import com.example.movieandro.databinding.FragmentTvShowBinding
import com.example.movieandro.ui.detail.DetailActivity
import splitties.fragments.start

class TvShowFragment : Fragment(), TvShowRecyclerViewListener {

    private lateinit var binding: FragmentTvShowBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentTvShowBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (activity != null) {
            val viewModel = ViewModelProvider(
                this,
                ViewModelProvider.NewInstanceFactory()
            )[TvShowViewModel::class.java]
            val tvShows = viewModel.getTvShows()

            val tvShowAdapter = TvShowAdapter()
            tvShowAdapter.setTvShows(tvShows)

            with(binding.rvTvshow) {
                layoutManager = LinearLayoutManager(context)
                setHasFixedSize(true)
                adapter = tvShowAdapter

                tvShowAdapter.listener = this@TvShowFragment
            }
        }
    }

    private fun onSelectedTvShow(tvShow: ShowEntity) {
        start<DetailActivity> {
            putExtra(DetailActivity.EXTRA_SHOW, tvShow.movieId)
        }
    }

    override fun onItemClicked(tvShow: ShowEntity) {
        onSelectedTvShow(tvShow)
    }
}