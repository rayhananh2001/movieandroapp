package com.example.movieandro.ui.detail

import androidx.lifecycle.ViewModel
import com.example.movieandro.data.ShowEntity
import com.example.movieandro.utils.DataDummy

class DetailViewModel : ViewModel() {
    private lateinit var showId: String

    // ngambil id acara dari putExtra
    fun setSelectedShow(showId: String) {
        this.showId = showId
    }

    // ngambil acara sesuai ama id yg dikasih
    fun getShow(): ShowEntity? {
        var show: ShowEntity? = null
        for (showEntity in DataDummy.generateAllShow()) {
            if (showEntity.movieId == showId) show = showEntity
        }
        return show
    }
}